# Functional Programming in Java

This repository holds the exercises supporting the session `Functional Programming in Java` by JDriven.

The master branch holds both the exercises and the solutions. Do only look at the solutions when you are stuck!
