package com.jdriven.fpjava.solutions;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;

public class PureFunctionsTest {
    @Test
    public void doesNothingTest() {
        final var outContent = new ByteArrayOutputStream();
        final var originalOut = System.out;

        System.setOut(new PrintStream(outContent));

        PureFunctions.doesNothing();
        assertThat(outContent.toString()).isEmpty();

        System.setOut(originalOut);
    }

    /**
     * Please never write tests like these...
     * We only did this so everything would keep compiling even if you change the method signature
     */
    @Test
    public void pureCounterIncrease() throws NoSuchMethodException, NoSuchFieldException, InvocationTargetException, IllegalAccessException {
        final var pureFunctions = new PureFunctions();
        final var pureFunctionsClass = pureFunctions.getClass();
        final var increaseMethod = pureFunctionsClass.getMethod("increase", int.class);
        assertThat(increaseMethod.getReturnType()).isEqualTo(PureFunctions.Counter.class);

        final var counterClass = PureFunctions.Counter.class;
        final var incMethod = counterClass.getMethod("inc");
        final var valueField = counterClass.getDeclaredField("value");

        assertThat(incMethod.getReturnType()).isEqualTo(PureFunctions.Counter.class);
        assertThat(Modifier.isFinal(valueField.getModifiers())).isTrue();

        final var result = increaseMethod.invoke(pureFunctions, 0);

        assertThat(result).isEqualTo(new PureFunctions.Counter(1));
        assertThat(PureFunctions.increase(((PureFunctions.Counter) result).getValue())).isEqualTo(new PureFunctions.Counter(2));
    }
}
