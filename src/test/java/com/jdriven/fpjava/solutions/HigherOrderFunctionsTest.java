package com.jdriven.fpjava.solutions;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class HigherOrderFunctionsTest {

    @Test
    void addThree() throws InvocationTargetException, IllegalAccessException {
        try {
            final var method = HigherOrderFunctions.class.getDeclaredMethod("addThree", int.class);
            fail("The `addThree` method should no longer exist");
        } catch (NoSuchMethodException e) {
            // should happen
        }

        final var five = HigherOrderFunctions.addPlusThree(1, 1);
        assertThat(five).isEqualTo(5);

        boolean hasFunctionArgument = false;
        hasFunctionArgument |= hasFunctionArgument(Function.class, int.class, int.class);
        hasFunctionArgument |= hasFunctionArgument(int.class, Function.class, int.class);
        hasFunctionArgument |= hasFunctionArgument(int.class, int.class, Function.class);
        if (!hasFunctionArgument) {
            fail("Hint: Add the java.util.function.Function somewhere...");
        }
    }

    private static boolean hasFunctionArgument(final Class<?> arg1, final Class<?> arg2, final Class<?> arg3) throws InvocationTargetException, IllegalAccessException {
        try {
            final var method = HigherOrderFunctions.class.getDeclaredMethod("add", arg1, arg2, arg3);
            method.setAccessible(true);
            final Function<? extends Number, ? extends Number> addThree = a -> a.intValue() + 3;
            Object answer;

            if (arg1.isAssignableFrom(Function.class)) {
                answer = method.invoke(null, addThree, 1, 1);
            } else if (arg2.isAssignableFrom(Function.class)) {
                answer = method.invoke(null, 1, addThree, 1);
            } else {
                answer = method.invoke(null, 1, 1, addThree);
            }

            if (answer instanceof Number n && n.intValue() != 5) {
                fail("Don't cheat! Use the java.util.function.Function you defined in the `add` method.");
            } else if (!(answer instanceof Number)) {
                fail("Hint: The signature of the java.util.function.Function argument in the `add` method is not correct. Use numbers...");
            }

            return true;
        } catch (NoSuchMethodException e) {
            return false;
        }
    }

    @Test
    void lazyRandom() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        final var method = HigherOrderFunctions.class.getMethod("lazyRandom");
        final var returnObj = method.invoke(null);

        switch (returnObj) {
            case null ->  fail("You did not start the assigment, didn't you?");
            case Supplier<?> s -> {
                if (!(s.get() instanceof Number)) {
                    fail("Hint: You are going in the right direction, but the function does not return a number.");
                }
            }
            case Function<?, ?> f -> {
                if (f.apply(null) instanceof Number) {
                    fail("Your solution is correct, but is there a better type to express `() -> Number`?");
                } else {
                    fail("Hint: You are going in the right direction, but the function does not return a numbe.r");
                }
            }
            default -> fail("Please, read the assigment again...");
        }
    }
}
