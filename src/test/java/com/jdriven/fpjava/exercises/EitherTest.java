package com.jdriven.fpjava.exercises;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class EitherTest {

    @Test
    void map() {
        assertThat(Either.right("hi").map(String::toUpperCase)).isEqualTo(Either.right("HI"));

        assertThat(Either.left("hi").map(Object::toString).map(String::toUpperCase)).isEqualTo(Either.left("hi"));
    }

    @Test
    void flatMap() {
        assertThat(Either.<Exception, String>right("hi").flatMap(EitherTest::parse).isLeft()).isTrue();
        assertThat(Either.<Exception, String>right("123").flatMap(EitherTest::parse)).isEqualTo(Either.right(123));

        var exception = new RuntimeException("error");
        assertThat(Either.<Exception, String>left(exception).flatMap(EitherTest::parse)).isEqualTo(Either.left(exception));
    }

    @Test
    void filter() {
        var nope = new RuntimeException("error");
        assertThat(Either.<Exception, String>right("hi").filter(it -> it.equals("hi"), nope).isRight()).isTrue();
        assertThat(Either.<Exception, String>right("hi").filter(it -> it.equals("hello"), nope).isLeft()).isTrue();

        var exception = new RuntimeException("error");
        assertThat(Either.<Exception, String>left(exception).filter(it -> it.equals("hi"), nope)).isEqualTo(Either.left(exception));
    }

    @Test
    void mapLeft() {
        assertThat(Either.<String, String>right("hi").mapLeft(String::toUpperCase)).isEqualTo(Either.right("hi"));

        assertThat(Either.<String, String>left("hi").mapLeft(String::toUpperCase)).isEqualTo(Either.left("HI"));
    }

    @Test
    void flatMapLeft() {
        assertThat(Either.<String, String>left("left").flatMapLeft(EitherTest::transformLeft)).isEqualTo(Either.left("LEFT"));

        assertThat(Either.<String, String>right("Right").flatMapLeft(EitherTest::transformLeft)).isEqualTo(Either.right("Right"));
    }

    @Test
    void filterLeft() {
        assertThat(Either.<Exception, String>right("hi").filterLeft(it -> it instanceof RuntimeException, "Right")).isEqualTo(Either.right("hi"));

        var exception = new RuntimeException("error");
        assertThat(Either.<Exception, String>left(exception).filterLeft(it -> it instanceof RuntimeException, "Right")).isEqualTo(Either.left(exception));
        assertThat(Either.<Exception, String>left(exception).filterLeft(it -> it instanceof IllegalAccessException, "Right")).isEqualTo(Either.right("Right"));
    }

    @Test
    void fold() {
        assertThat(Either.<String, String>left("left").<String>fold(String::toUpperCase, String::toUpperCase)).isEqualTo("LEFT");
        assertThat(Either.<String, String>right("right").<String>fold(String::toUpperCase, String::toUpperCase)).isEqualTo("RIGHT");
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> Either.left(new RuntimeException("left")).fold(e -> {
            throw e;
        }, Object::toString));
    }

    /**
     * BONUS: Implement
     */
    @Test
    void sequence() {
        var result = Either.sequence(List.of(Either.right(1), Either.right(2), Either.right(3)));

        assertThat(result).isEqualTo(Either.right(List.of(1, 2, 3)));

        var result2 = Either.sequence(List.of(Either.right(1), Either.left("hi"), Either.left("ho")));
        assertThat(result2).isEqualTo(Either.left("hi"));
    }

    /**
     * BONUS: Implement
     */
    @Test
    void traverse() {
        var result = Either.traverse(List.of(2, 2, 2, 4), i -> (i % 2 == 0) ? Either.right(i) : Either.left("not even"));

        assertThat(result).isEqualTo(Either.right(List.of(2, 2, 2, 4)));

        var result2 = Either.traverse(List.of(2, 2, 3, 4), i -> (i % 2 == 0) ? Either.right(i) : Either.left("not even"));
        assertThat(result2).isEqualTo(Either.left("not even"));
    }

    private static Either<Exception, Integer> parse(String value) {
        try {
            return Either.right(Integer.parseInt(value));
        } catch (Exception e) {
            return Either.left(e);
        }
    }

    private static Either<String, String> transformLeft(String value) {
        return Either.left(value.toUpperCase());
    }
}
