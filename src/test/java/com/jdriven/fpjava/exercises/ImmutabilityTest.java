package com.jdriven.fpjava.exercises;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

class ImmutabilityTest {

    @Test
    public void listAppendTest() {
        final var originalValues = Arrays.asList(1, 2, 3);
        final var list = new ArrayList<>(originalValues);
        final var newList = Immutability.functionalAppend(list, 4);

        final var expectedValues = Arrays.asList(1, 2, 3, 4);

        assertThat(newList).containsExactlyElementsOf(expectedValues);
        assertThat(originalValues).containsExactlyElementsOf(list);
        assertThat(list).isNotEqualTo(newList);
    }
}
