package com.jdriven.fpjava.exercises;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;

public class TypesTest {


    @Test
    public void returnStringTest() {
        final var result = Types.returnString("fp rocks!");

        assertThat(result).isNotNull();
        assertThat(result).isInstanceOf(String.class);
    }

    @Test
    public void returnListStringTest() {
        final var result = Types.returnListString(List.of("fp rocks!"));
        assertThat(result).isNotNull();
    }

    @Test
    public void returnListTest() {
        final var result = Types.returnList(List.of(1337));
        assertThat(result).isNotNull();
        assertThat(result)
            .has(anyOf(
                new Condition<>(List::isEmpty, "List is empty"),
                new Condition<>(list -> List.of(1337).equals(list), "List is same as input")
            ));
    }

    @Test
    public void transformListStringTest() {
        final var result = Types.transformListString(List.of("fp rocks!"), str -> str + " It really does!");
        assertThat(result).isNotNull();
    }

    @Test
    public void transformListTest() {
        final var result = Types.transformList(List.of("fp rocks!"), String::length);
        assertThat(result).isNotNull();
        assertThat(result)
            .has(anyOf(
                new Condition<>(List::isEmpty, "List is empty"),
                new Condition<>(list -> List.of(9).equals(list), "List contains transformed values")
            ));
    }

    @Test
    public void getBTest() {
        final var result = Types.getB(Optional.of("fp rocks!"), String::length);
        assertThat(result).isNotNull();
        assertThat(result)
            .has(anyOf(
                new Condition<>(Optional::isEmpty, "Optional is empty"),
                new Condition<>(opt -> Optional.of(9).equals(opt), "Optional contains transformed value")
            ));
    }
}
