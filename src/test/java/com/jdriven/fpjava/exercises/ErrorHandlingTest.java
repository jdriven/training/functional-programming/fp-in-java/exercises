package com.jdriven.fpjava.exercises;

import io.vavr.control.Either;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.function.Consumer;

import static com.jdriven.fpjava.exercises.ErrorHandling.Employer;
import static com.jdriven.fpjava.exercises.ErrorHandling.User;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class ErrorHandlingTest {
    private final static User FIRST_USER = new User(1, "Chiel", Optional.of("JDriven").map(Employer::new));

    @Test
    public void divideTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final var divide = ErrorHandling.class.getMethod("divide", int.class, int.class);
        final var divideByZeroResult = divide.invoke(null, 30, 0);
        runAssertion(divideByZeroResult,
                either -> {
                    assertThat(either.isRight()).isFalse();
                    assertThat(either.getLeft()).isInstanceOf(ArithmeticException.class);
                },
                option -> {
                    assertThat(option.isEmpty()).isTrue();
                    assertThat(option).isEqualTo(Option.none());
                },
                optional -> assertThat(optional.isEmpty()).isTrue()
        );

        final var divideResult = divide.invoke(null, 30, 5);
        runAssertion(divideResult,
                either -> {
                    assertThat(either.isLeft()).isFalse();
                    assertThat(either.get()).isEqualTo(6);
                },
                option -> assertThat(option.get()).isEqualTo(6),
                optional -> assertThat(optional.get()).isEqualTo(6)
        );
    }

    @Test
    public void getUserTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final var getUser = ErrorHandling.class.getMethod("getUser", int.class);
        final var firstUserResult = getUser.invoke(null, 1);
        runAssertion(firstUserResult,
                either -> {
                    assertThat(either.isLeft()).isFalse();
                    assertThat(either.get()).isEqualTo(FIRST_USER);
                },
                option -> assertThat(option.get()).isEqualTo(FIRST_USER),
                optional -> assertThat(optional.get()).isEqualTo(FIRST_USER));

        final var noUserResult = getUser.invoke(null, 1337);
        runAssertion(noUserResult,
                either -> assertThat(either.isLeft()).isTrue(),
                option -> assertThat(option.isEmpty()).isTrue(),
                optional -> assertThat(optional.isEmpty()).isTrue());
    }

    @Test
    public void getEmployerTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final var getUserEmployer = ErrorHandling.class.getMethod("getUserEmployer", int.class);
        final var firstUserResult = getUserEmployer.invoke(null, 1);
        runAssertion(firstUserResult,
                either -> {
                    assertThat(either.isLeft()).isFalse();
                    assertThat(either.get()).isEqualTo(FIRST_USER.employer().get());
                },
                option -> assertThat(option.get()).isEqualTo(FIRST_USER.employer().get()),
                optional -> assertThat(optional.get()).isEqualTo(FIRST_USER.employer().get()));

        final var noUserResult = getUserEmployer.invoke(null, 1337);
        runAssertion(noUserResult,
                either -> assertThat(either.isLeft()).isTrue(),
                option -> assertThat(option.isEmpty()).isTrue(),
                optional -> assertThat(optional.isEmpty()).isTrue());
    }

    private void runAssertion(Object result,
                              Consumer<Either<?, ?>> eitherAssertion,
                              Consumer<Option<?>> optionAssertion,
                              Consumer<Optional<?>> optionalAssertion) {
        assertThat(result).isNotNull();
        assertThat(result).isInstanceOfAny(Either.class, Option.class, Optional.class);
        switch (result) {
            case Either<?, ?> either -> eitherAssertion.accept(either);
            case Option<?> option -> optionAssertion.accept(option);
            case Optional<?> optional -> optionalAssertion.accept(optional);
            default -> fail("Unknown return type");
        }
    }
}
