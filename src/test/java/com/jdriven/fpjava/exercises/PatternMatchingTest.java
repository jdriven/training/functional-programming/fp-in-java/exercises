package com.jdriven.fpjava.exercises;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

import static com.jdriven.fpjava.exercises.PatternMatching.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PatternMatchingTest {
    @Test
    public void testMaxSpeedForACar() {
        Car car = new Car(2);

        assertEquals(18.5, calculateMaxSpeed(car));
        assertHasCorrectMethodsAndHierarchy(car);
    }

    @Test
    public void testMaxSpeedForABike() {
        Bike bike1 = new Bike(0, 3);
        assertEquals(1.5, calculateMaxSpeed(bike1));

        Bike bike2 = new Bike(2, 3);
        assertEquals(3.0, calculateMaxSpeed(bike2));

        assertHasCorrectMethodsAndHierarchy(bike1);
    }

    @Test
    public void testMaxSpeedForAPedestrian() {
        Pedestrian pedestrian = new Pedestrian(2.0);
        assertEquals(25.0, calculateMaxSpeed(pedestrian));

        assertHasCorrectMethodsAndHierarchy(pedestrian);
    }

    @Test
    public void bonusPatternMatchHasTheCorrectAmountOfCases() throws Exception {
        var filePath = Paths.get("src/main/java/com/jdriven/fpjava/exercises/PatternMatching.java");
        var caseLines = Files.readAllLines(filePath).stream()
                .dropWhile(line -> !line.contains("static double calculateMaxSpeed"))
                .filter(line -> line.contains("case"))
                .toList();

        assertAll(
                () -> assertThat(caseLines).hasSize(4),
                () -> assertThat(caseLines.stream().filter(line -> line.contains("when")).count()).isGreaterThan(0)
        );
    }

    private static void assertHasCorrectMethodsAndHierarchy(TransportationMethod transportationMethod) {
        var instanceOfResult = Arrays.stream(Optional.ofNullable(TransportationMethod.class.getPermittedSubclasses()).orElse(new Class<?>[0]))
                .filter(clazz -> clazz.equals(transportationMethod.getClass()))
                .count();

        var methodCountResult = Arrays.stream(transportationMethod.getClass().getMethods()).filter(m -> m.getName().equals("maxSpeed")).count();

        assertAll(
                () -> assertThat(TransportationMethod.class.isSealed()).isTrue(),
                () -> assertThat(instanceOfResult).isEqualTo(1),
                () -> assertThat(methodCountResult).isEqualTo(0)
        );
    }


}