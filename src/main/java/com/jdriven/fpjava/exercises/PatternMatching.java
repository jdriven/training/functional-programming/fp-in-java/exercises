package com.jdriven.fpjava.exercises;

public class PatternMatching {

    /**
     * Exercise 1: make this interface sealed
     * Exercise 2: refactor the implementation of {@link PatternMatching#calculateMaxSpeed} to use pattern matching.
     * Use the interface method maxSpeed() to calculate the max speed on each match.
     * Exercise 3: remove the interface method maxSpeed() and implement the logic for each record in a case in the calculateMaxSpeed method.
     * (At this point, the 3 out of 4 tests should pass.)
     * Exercise 4 (BONUS): refactor the cases, so that none of the code after a match uses if-statements. (using 'when' in a case).
     * (makes the last test pass)
     */
    interface TransportationMethod {
        double maxSpeed();
    }

    record Car(int gears) implements TransportationMethod {
        @Override
        public double maxSpeed() {
            return gears * 9.25d;
        }
    }

    record Bike(int frontGears, int rearGears) implements TransportationMethod {
        @Override
        public double maxSpeed() {
            var rearSpeed = 0.5d * rearGears;
            if (frontGears <= 0)
                return rearSpeed;
            else {
                return frontGears * rearSpeed;
            }
        }
    }

    record Pedestrian(double legLength) implements TransportationMethod {
        @Override
        public double maxSpeed() {
            return legLength * 12.5d;
        }
    }

    static double calculateMaxSpeed(TransportationMethod transportationMethod) {
        return transportationMethod.maxSpeed();
    }
}
