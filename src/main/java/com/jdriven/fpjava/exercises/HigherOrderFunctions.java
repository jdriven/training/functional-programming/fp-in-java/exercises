package com.jdriven.fpjava.exercises;

public class HigherOrderFunctions {

    /**
     * Exercise 1: Rewrite this method by replacing the `addThree` method by an inline function.
     * Pass this function to {@link HigherOrderFunctions#add}, and apply it to the result.
     */
    public static int addPlusThree(int a, int b) {
        return addThree(add(a, b));
    }

    /**
     * Exercise 2: Return a function which supplies a random number.
     */
    public static Object lazyRandom() {
        return null;
    }

    // Hint: Remove this
    private static int addThree(int a) {
        return a + 3;
    }

    // Exercise 1: Change signature
    // Hint: Use {@link java.util.function.Function}.
    private static int add(int a, int b) {
        return a + b;
    }
}
