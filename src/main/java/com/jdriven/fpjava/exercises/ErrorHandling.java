package com.jdriven.fpjava.exercises;

import io.vavr.control.Either;
import io.vavr.control.Option;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class ErrorHandling {

    /**
     * Exercise 1: make this function total by handling its {@link ArithmeticException} in a functional way.
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
    public static Integer divide(int a, int b) {
        return a / b;
    }

    /**
     * Exercise 2: make this function total by handling its null on absence.
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
    public static User getUser(int userId) {
        return userDatabase.get(userId);
    }

    /**
     * Exercise 3: make this function total. No Exceptions!
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
    public static Employer getUserEmployer(int userId) {
        return userDatabase.get(userId).employer.get();
    }

    private static final Map<Integer, User> userDatabase =
            Stream.of(
                    new User(1, "Chiel", Optional.of("JDriven").map(Employer::new)),
                    new User(2, "Erik", Optional.of("JDriven").map(Employer::new)),
                    new User(3, "Deniz", Optional.of("JDriven").map(Employer::new)),
                    new User(4, "Jacob", Optional.of("JDriven").map(Employer::new))
            ).collect(toMap(User::userId, Function.identity()));


    public record User(int userId,
                       String name,
                       Optional<Employer> employer) {
    }

    public record Employer(String value) {
    }
}
