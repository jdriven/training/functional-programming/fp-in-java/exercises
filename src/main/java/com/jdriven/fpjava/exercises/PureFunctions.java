package com.jdriven.fpjava.exercises;

import lombok.AllArgsConstructor;

public class PureFunctions {

    /**
     * Exercise 1: make this function Pure
     * bonus: what can you return so the side-effect isn't lost?
     */
    public static void doesNothing() {
        System.out.println("I do nothing, or do I?");
    }

    /**
     * Exercise 2: make this function pure by returning {@link Counter}
     * Also see below, this exercise consists of two parts which you need to modify.
     */
    public static Integer increase(final int from) {
        var result = new Counter(from);
        return result.inc();
    }

    /**
     * Exercise 2: make this {@link Counter} pure by returning {@link Counter} and using a record.
     */
    @AllArgsConstructor
    static class Counter {
        private Integer value;

        public Integer inc() {
            value++;
            return value;
        }

        public Integer getValue() {
            return value;
        }
    }
}
