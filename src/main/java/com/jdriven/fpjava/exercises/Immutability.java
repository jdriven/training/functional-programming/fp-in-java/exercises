package com.jdriven.fpjava.exercises;

import java.util.List;

public class Immutability {

    /**
     * Exercise 1: apply immutability principles to this method, to make its test pass.
     * There are multiple approaches to solve this problem.
     * <p>
     * Hint: First try it using the plain java, then try it out with the {@link io.vavr.collection.List}.
     */
    public static <A> List<A> functionalAppend(final List<A> list, final A value){
        list.add(value);
        return list;
    }
}
