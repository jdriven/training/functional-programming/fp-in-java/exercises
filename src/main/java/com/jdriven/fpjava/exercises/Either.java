package com.jdriven.fpjava.exercises;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public sealed interface Either<L, R> extends com.jdriven.fpjava.contract.Either<L, R> {

    static <L, R> Either<L, R> right(R value) {
        return null;
    }

    static <L, R> Either<L, R> left(L value) {
        return null;
    }


    static <L, R> Either<L, List<R>> sequence(List<Either<L, R>> l) {
        return null;
    }

    static <A, L, R> Either<L, List<R>> traverse(List<A> l, Function<A, Either<L, R>> f) {
        return null;
    }

    record Right<L, R>(R value) implements Either<L, R> {

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        /**
         * BONUS: Implement methods for Right
         */

        @Override
        public <L2 extends L> com.jdriven.fpjava.contract.Either<L, R> filter(final Predicate<R> p, final L2 leftValue) {
            return null;
        }

        @Override
        public <R2 extends R> com.jdriven.fpjava.contract.Either<L, R> filterLeft(final Predicate<L> p, final R2 rightValue) {
            return null;
        }

        @Override
        public <A> A fold(final Function<L, A> lMap, final Function<R, A> rMap) {
            return null;
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> mapLeft(final Function<L, L2> f) {
            return null;
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> flatMap(final Function<R, com.jdriven.fpjava.contract.Either<L, R2>> f) {
            return null;
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> map(final Function<R, R2> f) {
            return null;
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> flatMapLeft(final Function<L, com.jdriven.fpjava.contract.Either<L2, R>> f) {
            return null;
        }
    }

    record Left<L, R>(L value) implements Either<L, R> {

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        /**
         * BONUS: Implement methods for left
         */

        @Override
        public <L2 extends L> com.jdriven.fpjava.contract.Either<L, R> filter(final Predicate<R> p, final L2 leftValue) {
            return null;
        }

        @Override
        public <R2 extends R> com.jdriven.fpjava.contract.Either<L, R> filterLeft(final Predicate<L> p, final R2 rightValue) {
            return null;
        }

        @Override
        public <A> A fold(final Function<L, A> lMap, final Function<R, A> rMap) {
            return null;
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> mapLeft(final Function<L, L2> f) {
            return null;
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> flatMap(final Function<R, com.jdriven.fpjava.contract.Either<L, R2>> f) {
            return null;
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> map(final Function<R, R2> f) {
            return null;
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> flatMapLeft(final Function<L, com.jdriven.fpjava.contract.Either<L2, R>> f) {
            return null;
        }
    }
}
