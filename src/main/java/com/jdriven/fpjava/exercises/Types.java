package com.jdriven.fpjava.exercises;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class Types {

    /**
     * Exercise 1: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static String returnString(final String value){
        return null;
    }

    /**
     * Exercise 2: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static List<String> returnListString(final List<String> list) {
        return null;
    }

    /**
     * Exercise 3: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A> List<A> returnList(final List<A> list) {
        return null;
    }

    /**
     * Exercise 4 in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static List<String> transformListString(final List<String> list, final Function<String, String> f) {
        return null;
    }

    /**
     * Exercise 5: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A, B> List<B> transformList(final List<A> list, final Function<A, B> f) {
        return null;
    }

    /**
     * Exercise 7: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A, B> Optional<B> getB(final Optional<A> optionalA, final Function<A, B> f) {
        return null;
    }
}
