package com.jdriven.fpjava.solutions;

/**
 * Exercise 1: make this interface sealed
 * Exercise 2: refactor the implementation of this method to use pattern matching. Use the interface method maxSpeed() to calculate the max speed on each match.
 * Exercise 3: remove the interface method maxSpeed() and implement the logic for each record in a case in the calculateMaxSpeed method.
 * Exercise 4 (BONUS): refactor the cases, so that none of the code after a match uses if-statements. (using 'when' in a case).
 */
public class PatternMatching {
    sealed interface TransportationMethod {
    }

    record Car(int gears) implements TransportationMethod {
    }

    record Bike(int frontGears, int rearGears) implements TransportationMethod {
    }

    record Pedestrian(double legLength) implements TransportationMethod {
    }

    static double calculateMaxSpeed(TransportationMethod transportationMethod) {
        return switch (transportationMethod) {
            case Car car -> car.gears * 9.25d;
            case Bike bike when bike.frontGears > 0 -> bike.frontGears * bike.rearGears * 0.5d;
            case Bike bike -> bike.rearGears * 0.5d;
            case Pedestrian pedestrian -> pedestrian.legLength * 12.5d;
        };
    }
}
