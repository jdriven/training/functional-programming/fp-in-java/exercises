package com.jdriven.fpjava.solutions;

public class PureFunctions {

    /**
     * Exercise 1: make this function Pure
     * bonus: what can you return so the side-effect isn't lost?
     */
    public static void doesNothing() {
        // System.out.println("I do nothing, or do I?");
    }

    /**
     * This would be the solution for the bonus. This does not lose the side effect, but does not execute it either.
     * Leaving it up to the caller when or how to execute it.
     */
    public static Runnable doesNothingAlternative() {
        return () -> System.out.println("I do nothing, or do I?");
    }

    /**
     * Exercise 2: make this function pure by returning {@link Counter}
     * Also see below, this exercise consists of two parts which you need to modify.
     */
    public static Counter increase(final int from) {
        var result = new Counter(from);
        return result.inc();
    }

    /**
     * Exercise 2: make this {@link Counter} pure by returning {@link Counter} and using a record.
     */
    public record Counter(Integer value) {
        public Counter inc() {
            return new Counter(value + 1);
        }

        public Integer getValue() {
            return value;
        }
    }
}
