package com.jdriven.fpjava.solutions;

import java.util.function.Function;
import java.util.function.Supplier;

public class HigherOrderFunctions {

    /**
     * Exercise 1: Rewrite this method by replacing the `addThree` method by an inline function.
     * Pass this function to {@link HigherOrderFunctions#add}, and apply it to the result.
     */
    public static int addPlusThree(int a, int b) {
        // return add(a, b, x -> x + 3);
        Function<Integer, Integer> addThree = x -> x + 3;
        return add(a, b, addThree);
    }

    /**
     *  Exercise 2: Return a function which supplies a random number.
     */
    public static Supplier<Double> lazyRandom() {
        //return () -> Math.random();
        return Math::random;
    }

    private static int add(int a, int b, Function<Integer, Integer> f) {
        return f.apply(a + b);
    }
}
