package com.jdriven.fpjava.solutions;

import io.vavr.control.Either;
import io.vavr.control.Option;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class ErrorHandling {

    /**
     * Exercise 1: make this function total by handling its {@link ArithmeticException} in a functional way.
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
//    public static Optional<Integer> divide(int a, int b) {
//    public static Option<Integer> divide(int a, int b) {
    public static Either<Exception, Integer> divide(int a, int b) {
        // return Optional.of(b).filter(testB -> testB != 0).map(nonZeroB -> a / nonZeroB); // filtering removes the need for try/catch

        try {
            // return Optional.of(a / b);
            // return Option.of(a / b);
            return Either.right(a / b);
        } catch (ArithmeticException ex) {
            // return Optional.empty();
            // return Option.none();
            return Either.left(ex);
        }
    }

    /**
     * Exercise 2: make this function total by handling its null on absence.
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
//    public static Optional<User> getUser(int userId) {
//    public static Option<User> getUser(int userId) {
    public static Either<UserException, User> getUser(int userId) {
        // return Optional.ofNullable(userDatabase.get(userId));
        // return Option.of(userDatabase.get(userId));
        return Option.of(userDatabase.get(userId)).toEither(UserNotFoundException::new);
    }

    /**
     * Exercise 3: make this function total. No Exceptions!
     * Hint: Change the return value by using {@link Either}, {@link Option}, or java's plain old {@link Optional}.
     */
//    public static Optional<Employer> getUserEmployer(int userId) {
//    public static Option<Employer> getUserEmployer(int userId) {
    public static Either<UserException, Employer> getUserEmployer(int userId) {
        // return getUser(userId).flatMap(User::employer);
        // return getUser(userId).flatMap(User::employer).map(Option::ofOptional);
        return getUser(userId)
                .map(User::employer).map(Option::ofOptional)
                .flatMap(employers -> employers.toEither(EmployerNotFoundException::new));
    }

    private static final Map<Integer, User> userDatabase =
            Stream.of(
                    new User(1, "Chiel", Optional.of("JDriven").map(Employer::new)),
                    new User(2, "Erik", Optional.of("JDriven").map(Employer::new)),
                    new User(3, "Deniz", Optional.of("JDriven").map(Employer::new)),
                    new User(4, "Jimmy", Optional.of("JDriven").map(Employer::new))
            ).collect(toMap(User::userId, Function.identity()));


    public record User(int userId,
                String name,
                Optional<Employer> employer) {
    }

    public record Employer(String value) {
    }

    public static class UserException extends Exception {
    }

    public static class UserNotFoundException extends UserException {
    }

    public static class EmployerNotFoundException extends UserException {
    }
}
