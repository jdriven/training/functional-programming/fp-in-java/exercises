package com.jdriven.fpjava.solutions;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Types {

    /**
     * Exercise 1: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static String returnString(final String value) {
        // return "any string will do, so you can complete this code in infinite ways";
        return value;
    }

    /**
     * Exercise 2: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static List<String> returnListString(final List<String> list) {
        // return List.of("any", "list", "with", "strings", "will", "do");
        // return Collections.emptyList();
        return list;
    }

    /**
     * Exercise 3: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A> List<A> returnList(final List<A> list) {
        // return Collections.emptyList();
        return list;
    }

    /**
     * Exercise 4: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static List<String> transformListString(final List<String> list, final Function<String, String> f) {
        // return List.of("any", "list", "with", "strings", "will", "do");
        // return Collections.emptyList();
        // return list;
        return list.stream().map(f).toList();
    }

    /**
     * Exercise 5: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A, B> List<B> transformList(final List<A> list, final Function<A, B> f) {
        // return Collections.emptyList();
        return list.stream().map(f).toList();
    }

    /**
     * Exercise 6: in how many ways could you complete this code?
     * There are a few rules:
     * * no null, or {@link NullPointerException}
     * * no mutability
     */
    public static <A, B> Optional<B> getB(final Optional<A> optionalA, final Function<A, B> f) {
        // return Optional.empty();
        return optionalA.map(f);
    }
}
