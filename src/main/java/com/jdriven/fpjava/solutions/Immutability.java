package com.jdriven.fpjava.solutions;

import java.util.List;

public class Immutability {

    /**
     * Exercise 1: apply immutability principles to this method, to make its test pass.
     * There are multiple approaches to solve this problem.
     * <p>
     * Hint: First try it using the plain java, then try it out with the {@link io.vavr.collection.List}.
     */
    public static <A> List<A> functionalAppend(final List<A> list, final A value){
        // A solution using Java Stream
        // return Stream.concat(list.stream(), Stream.of(value)).collect(Collectors.toList());
        return io.vavr.collection.List.ofAll(list).append(value).asJava();
    }
}
