package com.jdriven.fpjava.solutions;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public sealed interface Either<L, R> extends com.jdriven.fpjava.contract.Either<L, R> {

    static <L, R> Either<L, R> right(R value) {
        return new Right<>(value);
    }

    static <L, R> Either<L, R> left(L value) {
        return new Left<>(value);
    }

    static <L, R> com.jdriven.fpjava.contract.Either<L, List<R>> sequence(List<Either<L, R>> l) {
        return traverse(l, Function.identity());
    }

    static <A, L, R> com.jdriven.fpjava.contract.Either<L, List<R>> traverse(List<A> l, Function<A, Either<L, R>> f) {
        return l.stream()
                .map(f)
                .map(e -> e.map(List::of))
                .reduce(Either.right(List.of()), (e1, e2) -> e1.flatMap(r1 -> e2.map(r2 -> Stream.concat(r1.stream(), r2.stream()).toList())));
    }

    record Right<L, R>(R value) implements Either<L, R> {

        @Override
        public boolean isRight() {
            return true;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public <L2 extends L> com.jdriven.fpjava.contract.Either<L, R> filter(final Predicate<R> p, final L2 leftValue) {
            return p.test(value) ? this : new Left<>(leftValue);
        }

        @Override
        public <R2 extends R> com.jdriven.fpjava.contract.Either<L, R> filterLeft(final Predicate<L> p, final R2 rightValue) {
            return this;
        }

        @Override
        public <A> A fold(final Function<L, A> lMap, final Function<R, A> rMap) {
            return rMap.apply(value);
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> mapLeft(final Function<L, L2> f) {
            return new Right<>(value);
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> flatMap(final Function<R, com.jdriven.fpjava.contract.Either<L, R2>> f) {
            return f.apply(value);
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> map(final Function<R, R2> f) {
            return new Right<>(f.apply(value));
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> flatMapLeft(final Function<L, com.jdriven.fpjava.contract.Either<L2, R>> f) {
            return new Right<>(value);
        }
    }

    record Left<L, R>(L value) implements Either<L, R> {


        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public <L2 extends L> com.jdriven.fpjava.contract.Either<L, R> filter(final Predicate<R> p, final L2 leftValue) {
            return this;
        }

        @Override
        public <R2 extends R> com.jdriven.fpjava.contract.Either<L, R> filterLeft(final Predicate<L> p, final R2 rightValue) {
            return p.test(value) ? this : new Right<>(rightValue);
        }

        @Override
        public <A> A fold(final Function<L, A> lMap, final Function<R, A> rMap) {
            return lMap.apply(value);
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> mapLeft(final Function<L, L2> f) {
            return new Left<>(f.apply(value));
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> flatMap(final Function<R, com.jdriven.fpjava.contract.Either<L, R2>> f) {
            return new Left<>(value);
        }

        @Override
        public <R2> com.jdriven.fpjava.contract.Either<L, R2> map(final Function<R, R2> f) {
            return new Left<>(value);
        }

        @Override
        public <L2> com.jdriven.fpjava.contract.Either<L2, R> flatMapLeft(final Function<L, com.jdriven.fpjava.contract.Either<L2, R>> f) {
            return f.apply(value);
        }
    }
}
