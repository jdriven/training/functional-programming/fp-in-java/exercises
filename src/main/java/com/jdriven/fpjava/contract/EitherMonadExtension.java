package com.jdriven.fpjava.contract;

import java.util.function.Function;
import java.util.function.Predicate;

public interface EitherMonadExtension<L, R> extends EitherMonad<L, R>, EitherMonadLeft<L, R> {
    /**
     * Filters the R value of the Either. Returns L unchanged when R is absent, R unchanged when p passes, or a new L when R fails p.
     *
     * @param p         predicate to test the current R value, if it exists.
     * @param leftValue the left value when or @param p fails.
     * @return A filtered Either.
     */
    <L2 extends L> Either<L, R> filter(Predicate<R> p, L2 leftValue);

    /**
     * Filters the L value of the Either. Returns R unchanged when L is absent, L unchanged when p passes, or a new R2 when L fails p.
     *
     * @param p          predicate to test the current L value, if it exists.
     * @param rightValue the right value when @param p fails.
     * @return A filtered Either.
     */
    <R2 extends R> Either<L, R> filterLeft(Predicate<L> p, R2 rightValue);

    /**
     * Folds over the left or right value to produce the desired result type. Used to convert the Either to its underlying value.
     *
     * @param lMap
     * @param rMap
     * @param <A>
     * @return
     */
    <A> A fold(Function<L, A> lMap, Function<R, A> rMap);
}

interface EitherMonad<L, R> extends EitherFunctor<L, R> {
    /**
     * Sequentially transforms the R value of the Either. Returns L unchanged when R is absent, a new EitherP<R2> if R is present.
     *
     * @param f    the function to map value R to a new EitherP<L, R2>
     * @param <R2> the Right type of the new Either
     * @return a transformed Either on the Right value.
     */
    <R2> Either<L, R2> flatMap(Function<R, Either<L, R2>> f);

}

interface EitherFunctor<L, R> {
    /**
     * Transforms the Right value using f. Returns a new EitherP<R2> when R is present, or L unchanged otherwise.
     *
     * @param f    the function to map value R to R2
     * @param <R2> the Right type of the new Either
     * @return a transformed Either on the Right value.
     */
    <R2> Either<L, R2> map(Function<R, R2> f);

}

interface EitherMonadLeft<L, R> extends EitherFunctorLeft<L, R> {
    /**
     * Sequentially transforms the R value of the Either. Returns L unchanged when R is absent, a new EitherP<R2> if R is present.
     *
     * @param f    the function to map value L to a new EitherP<L2, R>
     * @param <L2> the Left type of the new Either
     * @return a transformed Either on the Right value.
     */
    <L2> Either<L2, R> flatMapLeft(Function<L, Either<L2, R>> f);
}

interface EitherFunctorLeft<L, R> {
    /**
     * Transforms the Left value using f. Returns a new EitherP<R2> when R is present, or L unchanged otherwise.
     *
     * @param f    the function to map value R to R2
     * @param <L2> the Left type of the new Either
     * @return a transformed Either on the Right value.
     */
    <L2> Either<L2, R> mapLeft(Function<L, L2> f);
}
