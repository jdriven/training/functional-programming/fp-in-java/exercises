package com.jdriven.fpjava.contract;

public interface Either<L, R> extends EitherMonadExtension<L, R> {

    boolean isRight();

    boolean isLeft();
}
